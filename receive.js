/**
 * Trong file send.js thì nó là Producer, đóng vai trò là gửi các
 * task vào queue. Còn ở receive.js sẽ có vai trò là Consumer.
 * Consumer sẽ lắng nghe message từ RabbitMQ, ko giống như publisher (Producer)
 * chỉ publishes duy nhất 1 message. Receive (Consumer) giữ cho consumer
 * tiếp tục lắng nghe các message và in ra chúng.
 */

/**
 * Chúng ta phải start Consumer trước khi publisher (Producer) chạy.
 * Đảm bảo hàng đợi tồn tại trước khi sử dụng message from it.
 */

/**
 * Chúng ta nói với server gửi message từ queue cho chúng ta. 
 */

const fs = require('fs');
const amqp = require('amqplib/callback_api');

/**
 * List API to call and use rabbitmq
 */

amqp.connect('amqp://localhost', (error0, connection) => {
  if (error0) {
    throw error0;
  }

  connection.createChannel((error1, channel) => {
    if (error1) {
      throw error1;
    }
    const queue = 'TruongTrang';
    channel.assertQueue(queue, { durable: false })

    console.log("[*] Waiting for messages in %s. To exit press CTRL+C", queue)
    channel.consume(queue, (msg) => {
      if (msg) {
        fs.writeFileSync(`./receive/${Math.random()}.json`, JSON.stringify(JSON.parse(msg.content)))
      }
      // console.log(`[x] Received ${msg.content.toString()}`);
    }, {
      noAck: true
    })
  })
})
