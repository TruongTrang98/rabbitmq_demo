/**
 * This is Producer, gửi các task vào queue
 */
const axios = require('axios');
const amqp = require('amqplib/callback_api');
const fs = require('fs');

const apis = [
  'https://world.openfoodfacts.org/api/v0/product/737628064502.json',
  'https://pokeapi.co/api/v2/pokemon?limit=10000000'
]

amqp.connect('amqp://localhost', (error0, connection) => {
  if (error0) {
    throw error0;
  }
  connection.createChannel((error1, chanel) => {
    if (error1) {
      throw error1;
    }

    const queue = 'TruongTrang';
    const msg = 'Xin chào Tráng';

    chanel.assertQueue(queue, { durable: false });

    //Cách 1
    // (async () => {
    //   let data = await Promise.all(apis.map(item => {
    //     return axios.get(item);
    //   }))
    //   data = data.map(i => i.data);
    //   console.log(data);
    //   data.forEach((item, idx) => {
    //     fs.writeFileSync(`./send/${idx}.json`, JSON.stringify(item))
    //   })
    //   chanel.sendToQueue(queue, Buffer.from(JSON.stringify({ ...data })));
    // })()


    //Cách 2
    // (async () => {
    //   for (let i of apis) {
    //     const result = await axios.get(i);
    //     chanel.sendToQueue(queue, Buffer.from(JSON.stringify(result.data)))
    //   }
    // })()


    //Cách 3
    const data = [];
    for (let i of apis) {
      data.push((() => axios.get(i))())
    }
    Promise.all(data)
      .then(result => {
        // result = result.map(i => i.data);
        result.forEach(i => {
          let data = i.data;
          chanel.sendToQueue(queue, Buffer.from(JSON.stringify(data)))
        })
        // chanel.sendToQueue(queue, Buffer.from(JSON.stringify(result)))
      })
  })
})